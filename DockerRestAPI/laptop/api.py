# Laptop Service

from flask import Flask, Response, request
from flask_restful import Resource, Api
import os
import flask
import pymongo
from pymongo import MongoClient

app = Flask(__name__)
api = Api(app)

client = MongoClient("db", 27017)
db = client.tododb

class ListAll(Resource):
    def get(self):
        top = request.args.get("top")
        if top == None:
		top = 20
        _items = db.tododb.find().sort("open", pymongo.ASCENDING).limit(int(top))
        items = [item for item in _items]
        return 
        {
            'open': [item['open_times'] for item in items], 'close': [item['close_times'] for item in items]
        }

class ListAllCsv(Resource):
    def get(self):
        top = request.args.get("top")
	    csvfile = ""	
        if top == None:
            top = 20
        _items = db.tododb.find().sort("open", pymongo.ASCENDING).limit(int(top))
        items = [item for item in _items]
	    for item in items:
            csvfile += item['open_times'] + ', ' + item['close_times'] + ', '
        return
            csvfile

class ListOpenOnly(Resource):
    def get(self):
        top = request.args.get("top")
        if top == None: 
            top = 20
        _items = db.tododb.find().sort("open", pymongo.ASCENDING).limit(int(top))
        return 
        {
            'open': [item['open_times'] for item in _items]
        }

class ListOpenOnlyCsv(Resource):
    def get(self):
        top = request.args.get("top")
	    csvfile = ""	
        if top == None:
		    top = 20
        _items = db.tododb.find().sort("open", pymongo.ASCENDING).limit(int(top))
        items = [item for item in _items]
	    for item in items:
	        csvfile += item['open_times'] + ', '
        return 
            csvfile

class ListCloseOnly(Resource):
    def get(self):
        top = request.args.get("top")
        if top == None: 
            top = 20
        _items = db.tododb.find().sort("close", pymongo.ASCENDING).limit(int(top))
        return 
	    {
            'close': [item['close_times'] for item in _items]
        }

class ListCloseOnlyCsv(Resource):
    def get(self):
        top = request.args.get("top")
	    csvfile = ""	
        if top == None:
            top = 20
        _items = db.tododb.find().sort("close", pymongo.ASCENDING).limit(int(top))
        items = [item for item in _items]
	    for item in items:
            csvfile += item['close_times'] + ', '
        return 
            csvfile

# Create routes
api.add_resource(ListAll, '/listAll', '/listAll/json')
api.add_resource(ListAllCsv, '/listAll/csv')

api.add_resource(ListOpenOnly, '/listOpenOnly', '/listOpenOnly/json')
api.add_resource(ListOpenOnlyCsv, '/listOpenOnly/csv')

api.add_resource(ListCloseOnly, '/listCloseOnly', '/listCloseOnly/json')
api.add_resource(ListCloseOnlyCsv, '/listCloseOnly/csv')

# Run the application
if __name__ == '__main__':
    app.run(host='0.0.0.0', port=80, debug=True)
