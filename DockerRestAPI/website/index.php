<html>
    <head>
        <title>Project 6</title>
    </head>

    <body>
        <h1>/listAll</h1>
        <ul>
            <?php
            $json = file_get_contents('http://laptop-service/listAll');
            
            $obj = json_decode($json);
	          $open = $obj->open;
	          $close = $obj->close;
	          
	    echo "Open:\n";
            foreach ($open as $k) 
            {
                echo "<li>$k</li>";
            }
            
            echo "Close:\n";
            foreach ($close as $k) 
            {
                echo "<li>$k</li>";
            }
            ?>
            
        <h1>/ListOpenOnly</h1>
            <?php
            $json = file_get_contents('http://laptop-service/listOpenOnly');
            $obj = json_decode($json);
	          $open = $obj->open;
	          
	    echo "Open:\n";
            foreach ($open as $k) 
            {
                echo "<li>$k</li>";
            }
            ?>
        
        <h1>/ListCloseOnly</h1>
            <?php
            $json = file_get_contents('http://laptop-service/listCloseOnly');
            $obj = json_decode($json);
	          $close= $obj->close;
	          
	    echo "Close:\n";
            foreach ($close as $k) 
            {
                echo "<li>$k</li>";
            }
            ?>
            
         <h1>/ListAll Top 3</h1>
            <?php
            $json = file_get_contents('http://laptop-service/listAll?top=3');
            $obj = json_decode($json);
	          $open = $obj->open;
		  $close = $obj->close;

	    echo "Top 3 Open Times:\n";
            foreach ($open as $k) 
            {
                echo "<li>$k</li>";
            }

            echo "Top 3 Close Times:\n";
            foreach ($close as $k) 
            {
                echo "<li>$k</li>";
            }
            ?>
            
         <h1>/ListOpenOnly Top 2</h1>
            <?php
            $json = file_get_contents('http://laptop-service/listOpenOnly/json?top=2');
            $obj = json_decode($json);
	          $open = $obj->open;
	          
	    echo "Top 2 Open Times:\n";
            foreach ($open as $k) 
            {
                echo "<li>$k</li>";
            }
            ?>

        <h1>/listAllCsv</h1>
            <?php
            echo file_get_contents('http://laptop-service/listAll/csv');
            ?>
            
        <h1>/listOpenOnlyCsv</h1>
            <?php
            echo file_get_contents('http://laptop-service/listOpenOnly/csv');
            ?>
            
        <h1>listCloseOnlyCsv</h1>
            <?php
            echo file_get_contents('http://laptop-service/listCloseOnly/csv');
            ?>
        
        </ul>
    </body>
</html>
